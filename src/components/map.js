import React from "react";

import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from "react-google-maps";
//-33.409014, -70.568927
const Map = (props) => {
  const { lat, lng } = props.marketSelect;

  const zoom = lat ? 15 : 11;
  return (
    <GoogleMap
      defaultZoom={zoom}
      defaultCenter={{
        lat: lat ? lat : -33.448256,
        lng: lng ? lng : -70.669419,
      }}
      center={{
        lat: lat ? lat : -33.448256,
        lng: lng ? lng : -70.669419,
      }}
      zoom={zoom}
    >
      {lat && lng && <Marker position={{ lat: lat, lng: lng }} />}
    </GoogleMap>
  );
};

export default withScriptjs(withGoogleMap(Map));
