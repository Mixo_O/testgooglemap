import React, { useState } from "react";
import PropTypes from "prop-types";
import { Layout, Menu } from "antd";

import { SearchOutlined } from "@ant-design/icons";

const { Item, SubMenu } = Menu;
const { Sider } = Layout;

const MenuSider = (props) => {
  const { rutas, onCambioMarket } = props;

  return (
    <Sider
      theme="dark"
      id="dashboard-panel"
      className="unselectable site-layout-background"
    >
      <Menu mode="inline" id="dashboard-panel-menu">
        {rutas.length &&
          rutas.map(({ title, lat, lng }, index) => (
            <Item
              key={index}
              title={title}
              className="dashboard-panel-item"
              onClick={() => {
                onCambioMarket(lat, lng);
              }}
            >
              <span>
                <SearchOutlined />
                {title}
              </span>
            </Item>
          ))}
      </Menu>
    </Sider>
  );
};

MenuSider.propTypes = {
  rutas: PropTypes.array.isRequired,
  onCambioMarket: PropTypes.func.isRequired,
};

export default MenuSider;
