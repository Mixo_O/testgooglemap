import React, { useState } from "react";
import Map from "./components/map";
import { Layout, Menu, Row, Col } from "antd";

import key from "./key";

import MenuSider from "./components/MenuSider";

const { Header, Content, Footer, Sider } = Layout;

//const mapURL = `https://maps.googleapis.com/maps/api/js?v=3.exp&key=${key.CLAVE_API}&callback=initMap`;
const mapURL = `https://maps.googleapis.com/maps/api/js?key=${key.CLAVE_API}`;
//-33.409014, -70.568927
const rutasMock = [
  { title: "CASA", lat: -33.409014, lng: -70.568927 },
  { title: "CASA1", lat: -32.409014, lng: -70.568927 },
  { title: "CASA2", lat: -31.409014, lng: -70.568927 },
  { title: "CASA3", lat: -30.409014, lng: -70.568927 },
  { title: "CASA4", lat: -29.409014, lng: -70.568927 },
];

function App() {
  const [rutas, setRutas] = useState(rutasMock);

  const [rutaSelect, setRutaSelect] = useState({});

  const onCambioMarket = (lat, lng) => {
    const obj = {
      lat,
      lng,
    };
    setRutaSelect(obj);
  };
  return (
    <Layout id="App">
      <MenuSider rutas={rutas} onCambioMarket={onCambioMarket} />
      <Content id="ContentApp">
        <Map
          googleMapURL={mapURL}
          marketSelect={rutaSelect}
          containerElement={<div style={{ height: "100vh", width: "100vw" }} />}
          mapElement={<div style={{ height: "100%" }} />}
          loadingElement={<p>Cargando...</p>}
        />
      </Content>
    </Layout>
  );
}

export default App;
